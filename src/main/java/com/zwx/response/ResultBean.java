package com.zwx.response;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 返回对象包装类（带泛型）
 *
 * @author 文希
 * @create 2019-08-16 17:07
 */
public class ResultBean<T> implements Serializable {
    /**
     * 操作是否成功
     */
    private boolean success;
    /**
     * 操作代码
     */
    private int code;
    /**
     * 提示信息
     */
    private String message;

    private String url;

    /**
     * 异常时间
     */
    private String date;

    /**
     * 总条数
     */
    private long count = 0;

    /**
     * 返回的数据
     */
    private T data;

    // 无参构造函数  封装成功参数
    public ResultBean() {
        setResultCode(ResultCode.SUCCESS);
    }

    //已知异常自定义消息（500/404等）
    public ResultBean(ResultCode resultCode, String message) {
        setResultCode(resultCode);
        this.message = message;
    }

    // 封装数据，默认封装resultCode。SUCCESS
    public ResultBean(T t) {
        setResultCode(ResultCode.SUCCESS);
        this.data = t;
    }

    // 封页封装
    public ResultBean(T lists, long total) {
        setResultCode(ResultCode.SUCCESS);
        this.data = lists;
        this.count = total;
    }

    // 封装未知异常
    public ResultBean(Throwable e) {
        setResultCode(ResultCode.UNKNOWN_EXCEPTION);
        this.message = e.getMessage();
    }

    private void setResultCode(ResultCode resultCode) {
        this.success = resultCode.success;
        this.code = resultCode.code;
        this.message = resultCode.message;
        this.date = LocalDateTime.now().toString();
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
