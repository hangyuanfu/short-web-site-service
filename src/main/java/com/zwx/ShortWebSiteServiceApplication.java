package com.zwx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShortWebSiteServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShortWebSiteServiceApplication.class, args);
    }

}
